// import $ = require('jquery');

(function($) {
    'use strict';
    // ===============================================
	// === Interfaces
	// ===============================================
    interface IDefaults {
        class: IDefaultsClass;
        tags: IDefaultsTags;
        template: IDefaultsTemplate;
    }
    interface IDefaultsClass {
        selected: string;
        unselected: string;
    }
    interface IDefaultsTags {
        inputs: string;
        inputSelected: string;
        inputUnselected: string;
        inputBtn: string;
    }
    interface IDefaultsTemplate {
        inputSelectedContent: string;
        inputUnselectedContent: string;
    }

    // ===============================================
	// === Class
	// ===============================================
    class BtnGroup {
        constructor(element: HTMLElement, options?: IDefaults) {
            this.element = element;
    		this.$elem = $(element);
    		this.options = $.extend(true, {}, BtnGroup.defaults, options);

    		this.$inputs = this.$elem.find(this.options.tags.inputs);
            this.$inputs.hide();

    		this._init();

    		this.$elem.data('BtnGroup', this);
        }

        // === Static ===
        static defaults: IDefaults = {
			class: {
				selected: 'btn-primary active',
				unselected: 'btn-default',
			},
			tags: {
				inputs: 'input',
				inputSelected: ':checked',
				inputUnselected: ':not(:checked)',
				inputBtn: 'label'
			},
			template: {
				inputSelectedContent: null,
				inputUnselectedContent: null
			}
		};
        // === Instance ===
        protected options: IDefaults;
        protected element: HTMLElement;
        protected $elem: JQuery;
        protected $inputs: JQuery;

        // === Public ===
        public update(): BtnGroup {
			let $inputs: JQuery;

			$inputs = this.$inputs.filter(this.options.tags.inputUnselected)
				.closest(this.options.tags.inputBtn)
				.addClass(this.options.class.unselected)
				.removeClass(this.options.class.selected);
			if (this.options.template.inputUnselectedContent !== null)
				$inputs.html(this.options.template.inputUnselectedContent);

			$inputs = this.$inputs.filter(this.options.tags.inputSelected)
				.closest(this.options.tags.inputBtn)
				.addClass(this.options.class.selected)
				.removeClass(this.options.class.unselected);
			if (this.options.template.inputSelectedContent !== null)
				$inputs.html(this.options.template.inputSelectedContent);

			return this;
		}

        // === Private ===
        // Init des évenements
        protected _initEvents(): BtnGroup {
            let self = this;

			// Si un des inputs change
			this.$inputs.on('change', function () {
				self.update();
			});

			return this;
        }
        // Inititalisation
        protected _init(): BtnGroup {
            this._initEvents()
				.update();

			return this;
        }
    }

    // ===============================================
	// === Plugin jQuery
	// ===============================================
	$.fn.btnGroup = function (options) {
		return this.each(function () {
			let $this = $(this);
			let BtnGroupInstance = $this.data('BtnGroup');

			if (options === 'destroy') {
				$this.data('BtnGroup', null);
			}
			else if (BtnGroupInstance instanceof BtnGroup) {
				BtnGroupInstance.update();
			}
			else
				new BtnGroup(this, options);
		});
	};
} (jQuery));
