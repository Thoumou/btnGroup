(function ($) {
    'use strict';
    var BtnGroup = (function () {
        function BtnGroup(element, options) {
            this.element = element;
            this.$elem = $(element);
            this.options = $.extend(true, {}, BtnGroup.defaults, options);
            this.$inputs = this.$elem.find(this.options.tags.inputs);
            this.$inputs.hide();
            this._init();
            this.$elem.data('BtnGroup', this);
        }
        BtnGroup.prototype.update = function () {
            var $inputs;
            $inputs = this.$inputs.filter(this.options.tags.inputUnselected)
                .closest(this.options.tags.inputBtn)
                .addClass(this.options.class.unselected)
                .removeClass(this.options.class.selected);
            if (this.options.template.inputUnselectedContent !== null)
                $inputs.html(this.options.template.inputUnselectedContent);
            $inputs = this.$inputs.filter(this.options.tags.inputSelected)
                .closest(this.options.tags.inputBtn)
                .addClass(this.options.class.selected)
                .removeClass(this.options.class.unselected);
            if (this.options.template.inputSelectedContent !== null)
                $inputs.html(this.options.template.inputSelectedContent);
            return this;
        };
        BtnGroup.prototype._initEvents = function () {
            var self = this;
            this.$inputs.on('change', function () {
                self.update();
            });
            return this;
        };
        BtnGroup.prototype._init = function () {
            this._initEvents()
                .update();
            return this;
        };
        BtnGroup.defaults = {
            class: {
                selected: 'btn-primary active',
                unselected: 'btn-default',
            },
            tags: {
                inputs: 'input',
                inputSelected: ':checked',
                inputUnselected: ':not(:checked)',
                inputBtn: 'label'
            },
            template: {
                inputSelectedContent: null,
                inputUnselectedContent: null
            }
        };
        return BtnGroup;
    }());
    $.fn.btnGroup = function (options) {
        return this.each(function () {
            var $this = $(this);
            var BtnGroupInstance = $this.data('BtnGroup');
            if (options === 'destroy') {
                $this.data('BtnGroup', null);
            }
            else if (BtnGroupInstance instanceof BtnGroup) {
                BtnGroupInstance.update();
            }
            else
                new BtnGroup(this, options);
        });
    };
}(jQuery));
//# sourceMappingURL=btngroup.js.map